﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ufuk_test.Context;
using ufuk_test.Models;

namespace ufuk_test.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ServiceController : Controller
    {
        private readonly TodoContext _context;

        public ServiceController(TodoContext context)
        {
            _context = context;
        }

        public IActionResult GetAll()
        {
            var todos = _context.Todos.ToArray();

            return Ok(todos);
        }

        [HttpPost]
        public IActionResult AddTodo(Todo newtodo)
        {
            Random rastgele = new Random();
            int sayi = rastgele.Next();
            //var newTodo = new Todo()
            //{
            //    Name = name,
            //    ID = sayi,
            //    Complate = false
            //};
            newtodo.ID = sayi;
           var todo = _context.Todos.Add(newtodo);
            _context.SaveChanges();
            return Ok(todo.Entity);
        }
    }
}